# **MoFEM (Cephas)** #

Is a next stage MoFEM development. Cephas is version of Joseph stripped from
unnecessary or obsolete implementation. Is a rock on which different finite
elements implementations are build. Is not full framework where whole FE
application is implemented.

The objective is to have implementation with reach abstraction (able to
implement nontrivial finite elements) while being small and extendable.

[Go to Documentation Page](http://mofem.eng.gla.ac.uk/mofem/html/index.html)

## Current build status

- ### **Drone.io**: [![Build Status](https://drone.io/bitbucket.org/likask/mofem-cephas/status.png)](https://drone.io/bitbucket.org/likask/mofem-cephas/latest)
- ### **CDashTesting**: [CDashTesting](http://cdash.eng.gla.ac.uk/cdash/)
