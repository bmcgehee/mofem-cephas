/** \file fluid_pressure_element.cpp

  \brief Atom test for fluid pressure element

*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
using namespace MoFEM;

namespace bio = boost::iostreams;
using bio::tee_device;
using bio::stream;

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  ErrorCode rval;
  PetscErrorCode ierr;

  PetscInitialize(&argc,&argv,(char *)0,help);

  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;

  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,1,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  const char *option;
  option = "";//"PARALLEL=BCAST;";//;DEBUG_IO";
  rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);

  //Create MoFEM (Joseph) database
  MoFEM::Core core(moab);
  MoFEM::Interface& m_field = core;

  //set entitities bit level
  BitRefLevel bit_level0;
  bit_level0.set(0);
  EntityHandle meshset_level0;
  rval = moab.create_meshset(MESHSET_SET,meshset_level0); CHKERRQ_MOAB(rval);
  ierr = m_field.seed_ref_level_3D(0,bit_level0); CHKERRQ(ierr);

  //Definitions

  //add DISPLACEMENT field, Hilbert space H1, veror field rank 3 (displacemnt
  //has three components ux,uy,uz)
  ierr = m_field.add_field("DISPLACEMENT",H1,3); CHKERRQ(ierr);


  //add entities on wich DISPLACEMENT field is approximated, you can add
  //entities form several approximation levels at onec. You can as well
  //approximate field only on some mesh subdomain, in that case displacements
  //are approximated on roor moab mesh.
  ierr = m_field.add_ents_to_field_by_TETs(0,"DISPLACEMENT"); CHKERRQ(ierr);

  //set app. order for displacement fiedl. it is set uniform approximation
  //order. in genreal evry entity can have arbitraty approximation level,
  //ranging from 1 to 10 and more.
  int order = 1;
  ierr = m_field.set_field_order(0,MBTET,"DISPLACEMENT",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBTRI,"DISPLACEMENT",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBEDGE,"DISPLACEMENT",order); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBVERTEX,"DISPLACEMENT",1); CHKERRQ(ierr);

  //define fluid pressure finite elements
  FluidPressure fluid_pressure_fe(m_field);
  fluid_pressure_fe.addNeumannFluidPressureBCElements("DISPLACEMENT");

  ///add probelem which will be solved, could be more than one problem
  //operating on some subset of defined approximatons spces
  ierr = m_field.add_problem("TEST_PROBLEM"); CHKERRQ(ierr);
  //mesh could have several refinment levels which share some subset of entities between them.
  //below defines on which set of entities (on refinment level 0) build approximation spaces for TEST_PROBLEM
  ierr = m_field.modify_problem_ref_level_add_bit("TEST_PROBLEM",bit_level0); CHKERRQ(ierr);

  //add finite element to test problem
  ierr = m_field.modify_problem_add_finite_element("TEST_PROBLEM","FLUID_PRESSURE_FE"); CHKERRQ(ierr);

  //construct data structures for fields and finite elements. at that points
  //entities, finite elements or dofs have unique uid, but are not partitioned
  //or numbered. user can add entities to mesh, add dofs or elements if
  //necessaery. in case of modifications data structures are updated.
  ierr = m_field.build_fields(); CHKERRQ(ierr);
  ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
  ierr = m_field.build_adjacencies(bit_level0); CHKERRQ(ierr);
  ierr = m_field.build_problems(); CHKERRQ(ierr);

  //to solve problem it need to be represented in matrix vector form. this
  //demand numeration of dofs and problem  partitioning.
  ierr = m_field.partition_simple_problem("TEST_PROBLEM"); CHKERRQ(ierr);
  ierr = m_field.partition_finite_elements("TEST_PROBLEM"); CHKERRQ(ierr);
  //what are ghost nodes, see Petsc Manual
  ierr = m_field.partition_ghost_dofs("TEST_PROBLEM"); CHKERRQ(ierr);

  //create vector for problem
  Vec F;
  ierr = m_field.VecCreateGhost("TEST_PROBLEM",ROW,&F); CHKERRQ(ierr);
  ierr = fluid_pressure_fe.setNeumannFluidPressureFiniteElementOperators("DISPLACEMENT",F,false,false); CHKERRQ(ierr);

  ierr = VecZeroEntries(F); CHKERRQ(ierr);
  ierr = m_field.loop_finite_elements("TEST_PROBLEM","FLUID_PRESSURE_FE",fluid_pressure_fe.getLoopFe()); CHKERRQ(ierr);
  ierr = VecAssemblyBegin(F); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(F); CHKERRQ(ierr);
  ierr = m_field.set_global_ghost_vector("TEST_PROBLEM",ROW,F,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);

  //ierr = VecView(F,PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

  // PetscViewer viewer;
  // ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"fluid_pressure_element.txt",&viewer); CHKERRQ(ierr);
  // ierr = VecChop(F,1e-4); CHKERRQ(ierr);
  // ierr = VecView(F,viewer); CHKERRQ(ierr);
  // ierr = PetscViewerDestroy(&viewer); CHKERRQ(ierr);

  double sum = 0;
  ierr = VecSum(F,&sum); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"sum  = %4.3e\n",sum); CHKERRQ(ierr);
  if(fabs(sum-1.0)>1e-8) {
    SETERRQ(PETSC_COMM_WORLD,MOFEM_ATOM_TEST_INVALID,"Failed to pass test");
  }
  double fnorm;
  ierr = VecNorm(F,NORM_2,&fnorm); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"fnorm  = %9.8e\n",fnorm); CHKERRQ(ierr);
  if(fabs(fnorm-6.23059402e-01)>1e-6) {
    SETERRQ(PETSC_COMM_WORLD,MOFEM_ATOM_TEST_INVALID,"Failed to pass test");
  }


  // std::map<EntityHandle,ublas::vector<double> > tags_vals;
  // for(_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field,"DISPLACEMENT",dof)) {
  //   tags_vals[dof->getEnt()].resize(3);
  //   tags_vals[dof->getEnt()][dof->getDofCoeffIdx()] = dof->getFieldData();
  // }
  // std::vector<EntityHandle> ents;
  // ents.resize(tags_vals.size());
  // std::vector<double> vals(3*tags_vals.size());
  // int idx = 0;
  // for(std::map<EntityHandle,ublas::vector<double> >::iterator mit = tags_vals.begin();
  //   mit!=tags_vals.end();mit++,idx++) {
  //   ents[idx] = mit->first;
  //   vals[3*idx + 0] = mit->second[0];
  //   vals[3*idx + 1] = mit->second[1];
  //   vals[3*idx + 2] = mit->second[2];
  // }
  //
  // double def_VAL[3] = {0,0,0};
  // Tag th_vals;
  // rval = moab.tag_get_handle("FLUID_PRESURE_FORCES",3,MB_TYPE_DOUBLE,th_vals,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL); CHKERRQ_MOAB(rval);
  // rval = moab.tag_set_data(th_vals,&ents[0],ents.size(),&vals[0]); CHKERRQ_MOAB(rval);
  //
  // EntityHandle out_meshset;
  // rval = moab.create_meshset(MESHSET_SET,out_meshset); CHKERRQ_MOAB(rval);
  // ierr = m_field.get_problem_finite_elements_entities("TEST_PROBLEM","FLUID_PRESSURE_FE",out_meshset); CHKERRQ(ierr);
  // rval = moab.write_file("out.vtk","VTK","",&out_meshset,1); CHKERRQ_MOAB(rval);
  // rval = moab.delete_entities(&out_meshset,1); CHKERRQ_MOAB(rval);

  //destroy vector
  ierr = VecDestroy(&F); CHKERRQ(ierr);

  ierr = PetscFinalize(); CHKERRQ(ierr);

  return 0;

}
