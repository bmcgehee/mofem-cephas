/** \file ThermalElement.cpp
  \ingroup mofem_thermal_elem
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
using namespace MoFEM;
#include <ThermalElement.hpp>

using namespace boost::numeric;

PetscErrorCode ThermalElement::OpGetGradAtGaussPts::doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
  PetscFunctionBegin;
  try {

    if(data.getIndices().size()==0) PetscFunctionReturn(0);
    int nb_dofs = data.getFieldData().size();
    int nb_gauss_pts = data.getN().size1();

    //initialize
    commonData.gradAtGaussPts.resize(nb_gauss_pts,3);
    if(type == MBVERTEX) {
      std::fill(commonData.gradAtGaussPts.data().begin(),commonData.gradAtGaussPts.data().end(),0);
    }

    for(int gg = 0;gg<nb_gauss_pts;gg++) {
      ublas::noalias(commonData.getGradAtGaussPts(gg)) += prod( trans(data.getDiffN(gg,nb_dofs)), data.getFieldData() );
    }

  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpThermalRhs::doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
  PetscFunctionBegin;

  if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
    PetscFunctionReturn(0);
  }

  try {

    if(data.getIndices().size()==0) PetscFunctionReturn(0);
    if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt())==dAta.tEts.end()) PetscFunctionReturn(0);

    PetscErrorCode ierr;

    int nb_row_dofs = data.getIndices().size();
    Nf.resize(nb_row_dofs);
    Nf.clear();
    //std::cerr << data.getIndices() << std::endl;
    //std::cerr << data.getDiffN() << std::endl;

    for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

      ublas::matrix<double>  val = dAta.cOnductivity_mat*getVolume()*getGaussPts()(3,gg);

      if(getHoGaussPtsDetJac().size()>0) {
        val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
      }

      //std::cerr << val << std::endl;
      //std::cerr << data.getDiffN() << std::endl;
      //std::cerr << data.getIndices() << std::endl;
      //std::cerr << commonData.gradAtGaussPts << std::endl;
      //cblas
      //cblas_dgemv(CblasRowMajor,CblasNoTrans,nb_row_dofs,3,val,
      //&data.getDiffN()(gg,0),3,&commonData.gradAtGaussPts(gg,0),1,
      //1.,&Nf[0],1);

      //ublas
      ublas::noalias(Nf) += prod(prod(data.getDiffN(gg,nb_row_dofs),val), commonData.getGradAtGaussPts(gg));

    }

    //std::cerr << Nf << std::endl;
    if(useTsF) {
      ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
    } else {
      ierr = VecSetValues(F,data.getIndices().size(),&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);

    }

  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpThermalLhs::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSurcesCore::EntData &row_data,
  DataForcesAndSurcesCore::EntData &col_data
) {
  PetscFunctionBegin;

  if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
    PetscFunctionReturn(0);
  }

  try {

    if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
    if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

    int nb_row = row_data.getN().size2();
    int nb_col = col_data.getN().size2();
    K.resize(nb_row,nb_col);
    K.clear();
    for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

      ublas::matrix<double>  val = dAta.cOnductivity_mat*getVolume()*getGaussPts()(3,gg);
      if(getHoGaussPtsDetJac().size()>0) {
        val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
      }

      //cblas
      //double *diff_N_row,*diff_N_col;
      //diff_N_row = &row_data.getDiffN()(gg,0);
      //diff_N_col = &col_data.getDiffN()(gg,0);
      //cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasTrans,
      //nb_row,nb_col,3,
      //val,diff_N_row,3,diff_N_col,3,1.,&K(0,0),nb_col);

      //ublas
      ublas::matrix<double> K1=prod(row_data.getDiffN(gg,nb_row),val);
      noalias(K) += prod(K1,trans(col_data.getDiffN(gg,nb_col)));
    }

    PetscErrorCode ierr;
    if(!useTsB) {
      const_cast<FEMethod*>(getFEMethod())->ts_B = A;
    }
    ierr = MatSetValues(
      (getFEMethod()->ts_B),
      nb_row,&row_data.getIndices()[0],
      nb_col,&col_data.getIndices()[0],
      &K(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(row_side != col_side || row_type != col_type) {
      transK.resize(nb_col,nb_row);
      noalias(transK) = trans( K );
      ierr = MatSetValues(
        (getFEMethod()->ts_B),
        nb_col,&col_data.getIndices()[0],
        nb_row,&row_data.getIndices()[0],
        &transK(0,0),ADD_VALUES
      ); CHKERRQ(ierr);
    }


  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpHeatCapacityRhs::doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
  PetscFunctionBegin;

  try {

    if(data.getIndices().size()==0) PetscFunctionReturn(0);
    int nb_row = data.getN().size2();
    Nf.resize(nb_row);
    Nf.clear();
    for(unsigned int gg = 0;gg<data.getN().size1();gg++) {
      double val = getGaussPts()(3,gg);
      if(getHoGaussPtsDetJac().size()>0) {
        val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
      }
      val *= commonData.temperatureRateAtGaussPts[gg];
      ////////////
      //cblas
      //cblas_daxpy(nb_row,val,&data.getN()(gg,0),1,&*Nf.data().begin(),1);
      //ublas
      ublas::noalias(Nf) += val*data.getN(gg);
    }
    Nf *= getVolume()*dAta.cApacity;
    PetscErrorCode ierr;
    ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
    &data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);


  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }


  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpHeatCapacityLhs::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSurcesCore::EntData &row_data,
  DataForcesAndSurcesCore::EntData &col_data
) {
  PetscFunctionBegin;

  try {

    if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
    if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

    int nb_row = row_data.getN().size2();
    int nb_col = col_data.getN().size2();
    M.resize(nb_row,nb_col);
    M.clear();

    for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

      double val = getGaussPts()(3,gg);
      if(getHoGaussPtsDetJac().size()>0) {
        val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
      }

      //cblas
      //double *N_row,*N_col;
      //N_row = &row_data.getN()(gg,0);
      //N_col = &col_data.getN()(gg,0);
      //cblas_dger(CblasRowMajor,
      //  nb_row,nb_col,val,N_row,1,N_col,1,&M(0,0),nb_col);
      //ublas
      noalias(M) += val*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) );

    }

    M *= getVolume()*dAta.cApacity*getFEMethod()->ts_a;

    PetscErrorCode ierr;
    ierr = MatSetValues(
      (getFEMethod()->ts_B),
      nb_row,&row_data.getIndices()[0],
      nb_col,&col_data.getIndices()[0],
      &M(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(row_side != col_side || row_type != col_type) {
      transM.resize(nb_col,nb_row);
      noalias(transM) = trans(M);
      ierr = MatSetValues(
        (getFEMethod()->ts_B),
        nb_col,&col_data.getIndices()[0],
        nb_row,&row_data.getIndices()[0],
        &transM(0,0),ADD_VALUES
      ); CHKERRQ(ierr);
    }


  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }


  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpHeatFlux::doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
  PetscFunctionBegin;

  if(data.getIndices().size()==0) PetscFunctionReturn(0);
  if(dAta.tRis.find(getNumeredEntFiniteElementPtr()->getEnt())==dAta.tRis.end()) PetscFunctionReturn(0);

  PetscErrorCode ierr;

  const FENumeredDofEntity *dof_ptr;
  ierr = getNumeredEntFiniteElementPtr()->getRowDofsByPetscGlobalDofIdx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
  int rank = dof_ptr->getNbOfCoeffs();

  int nb_dofs = data.getIndices().size()/rank;

  Nf.resize(data.getIndices().size());
  Nf.clear();
  //std::cerr << getNormal() << std::endl;
  //std::cerr << getNormals_at_GaussPt() << std::endl;

  for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

    double val = getGaussPts()(2,gg);
    double flux;
    if(hoGeometry) {
      double area = norm_2(getNormals_at_GaussPt(gg))*0.5; //cblas_dnrm2(3,&getNormals_at_GaussPt()(gg,0),1);
      flux = dAta.dAta.data.value1*area;  //FluxData.HeatFluxCubitBcData.data.value1 * area
    } else {
      flux = dAta.dAta.data.value1*getArea();
    }
    //cblas_daxpy(nb_row_dofs,val*flux,&data.getN()(gg,0),1,&*Nf.data().begin(),1);
    ublas::noalias(Nf) += val*flux*data.getN(gg,nb_dofs);

  }

  //std::cerr << "VecSetValues\n";
  //std::cerr << Nf << std::endl;
  //std::cerr << data.getIndices() << std::endl;

  if(useTsF||F==PETSC_NULL) {
    ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),
    &data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
  } else {
    ierr = VecSetValues(F,data.getIndices().size(),
    &data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpRadiationLhs::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSurcesCore::EntData &row_data,
  DataForcesAndSurcesCore::EntData &col_data
) {
  PetscFunctionBegin;

  PetscErrorCode ierr;

  try {

    if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
    if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

    int nb_row = row_data.getN().size2();
    int nb_col = col_data.getN().size2();

    N.resize(nb_row,nb_col);
    N.clear();

    for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {
      double T3_at_Gauss_pt = pow(commonData.temperatureAtGaussPts[gg],3.0);

      double radiationConst;
      if(hoGeometry) {
        double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
        radiationConst = dAta.sIgma*dAta.eMissivity*area;
      } else {
        radiationConst = dAta.sIgma*dAta.eMissivity*getArea();
      }
      const double fOur = 4.0;
      double val = fOur*getGaussPts()(2,gg)*radiationConst*T3_at_Gauss_pt;
      noalias(N) += val*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) );
    }

    if(!useTsB) {
      const_cast<FEMethod*>(getFEMethod())->ts_B = A;
    }
    ierr = MatSetValues(
      (getFEMethod()->ts_B),
      nb_row,&row_data.getIndices()[0],
      nb_col,&col_data.getIndices()[0],
      &N(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(row_side != col_side || row_type != col_type) {
      transN.resize(nb_col,nb_row);
      noalias(transN) = trans( N );
      ierr = MatSetValues(
        (getFEMethod()->ts_B),
        nb_col,&col_data.getIndices()[0],
        nb_row,&row_data.getIndices()[0],
        &transN(0,0),ADD_VALUES
      ); CHKERRQ(ierr);
    }

  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpRadiationRhs::doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
  PetscFunctionBegin;

  if(data.getIndices().size()==0) PetscFunctionReturn(0);
  if(dAta.tRis.find(getNumeredEntFiniteElementPtr()->getEnt())==dAta.tRis.end()) PetscFunctionReturn(0);

  PetscErrorCode ierr;

  const FENumeredDofEntity *dof_ptr;
  ierr = getNumeredEntFiniteElementPtr()->getRowDofsByPetscGlobalDofIdx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
  int rank = dof_ptr->getNbOfCoeffs();
  int nb_row_dofs = data.getIndices().size()/rank;

  Nf.resize(data.getIndices().size());
  Nf.clear();
  //std::cerr << getNormal() << std::endl;
  //std::cerr << getNormals_at_GaussPt() << std::endl;

  for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

    double T4_at_Gauss_pt = pow(commonData.temperatureAtGaussPts[gg],4.0);
    double ambientTemp = pow(dAta.aMbienttEmp,4.0);
    double tEmp = 0;

    if(ambientTemp > 0) {
      tEmp = -ambientTemp + T4_at_Gauss_pt;
    }

    double val = getGaussPts()(2,gg);
    double radiationConst;

    if(hoGeometry) {
      double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
      radiationConst = dAta.sIgma*dAta.eMissivity*tEmp*area;
    } else {
      radiationConst = dAta.sIgma*dAta.eMissivity*tEmp*getArea();
    }
    ublas::noalias(Nf) += val*radiationConst*data.getN(gg,nb_row_dofs);

  }

  //std::cerr << "VecSetValues\n";
  //std::cerr << Nf << std::endl;
  //std::cerr << data.getIndices() << std::endl;

  if(useTsF) {
    ierr = VecSetValues(getFEMethod()->ts_F,data.getIndices().size(),&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
  } else {
    ierr = VecSetValues(F,data.getIndices().size(),&data.getIndices()[0],&Nf[0],ADD_VALUES); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpConvectionRhs::doWork(
  int side,EntityType type,DataForcesAndSurcesCore::EntData &data
) {
  PetscFunctionBegin;

  if(data.getIndices().size()==0) PetscFunctionReturn(0);
  if(dAta.tRis.find(getNumeredEntFiniteElementPtr()->getEnt())==dAta.tRis.end()) PetscFunctionReturn(0);

  PetscErrorCode ierr;

  const FENumeredDofEntity *dof_ptr;
  ierr = getNumeredEntFiniteElementPtr()->getRowDofsByPetscGlobalDofIdx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
  int rank = dof_ptr->getNbOfCoeffs();

  int nb_row_dofs = data.getIndices().size()/rank;

  Nf.resize(data.getIndices().size());
  Nf.clear();
  //std::fill(Nf.begin(),Nf.end(),0);
  //std::cerr << getNormal() << std::endl;
  //std::cerr << getNormals_at_GaussPt() << std::endl;

  for(unsigned int gg = 0;gg<data.getN().size1();gg++) {

    double T_at_Gauss_pt = commonData.temperatureAtGaussPts[gg];
    double convectionConst;
    if(hoGeometry) {
      double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
      convectionConst = dAta.cOnvection*area*(T_at_Gauss_pt-dAta.tEmperature);
    } else {
      convectionConst = dAta.cOnvection*getArea()*(T_at_Gauss_pt-dAta.tEmperature);
    }
    double val = getGaussPts()(2,gg)*convectionConst;
    ublas::noalias(Nf) += val*data.getN(gg,nb_row_dofs);

  }

  //std::cerr << "VecSetValues\n";
  //std::cerr << Nf << std::endl;
  //std::cerr << data.getIndices() << std::endl;

  if(useTsF) {
    ierr = VecSetValues(
      getFEMethod()->ts_F,
      data.getIndices().size(),
      &data.getIndices()[0],&Nf[0],ADD_VALUES
    ); CHKERRQ(ierr);
  } else {
    ierr = VecSetValues(
      F,
      data.getIndices().size(),
      &data.getIndices()[0],
      &Nf[0],
      ADD_VALUES
    ); CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::OpConvectionLhs::doWork(
  int row_side,int col_side,
  EntityType row_type,EntityType col_type,
  DataForcesAndSurcesCore::EntData &row_data,
  DataForcesAndSurcesCore::EntData &col_data
) {
  PetscFunctionBegin;

  try {

    if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
    if(col_data.getIndices().size()==0) PetscFunctionReturn(0);

    int nb_row = row_data.getN().size2();
    int nb_col = col_data.getN().size2();
    K.resize(nb_row,nb_col);
    K.clear();

    for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {

      double convectionConst;
      if(hoGeometry) {
        double area = norm_2(getNormals_at_GaussPt(gg))*0.5;
        convectionConst = dAta.cOnvection*area;
      }   else {
        convectionConst = dAta.cOnvection*getArea();
      }
      double val = getGaussPts()(2,gg)*convectionConst;
      noalias(K) += val*outer_prod( row_data.getN(gg,nb_row),col_data.getN(gg,nb_col) );

    }

    PetscErrorCode ierr;
    if(!useTsB) {
      const_cast<FEMethod*>(getFEMethod())->ts_B = A;
    }
    ierr = MatSetValues(
      (getFEMethod()->ts_B),
      nb_row,&row_data.getIndices()[0],
      nb_col,&col_data.getIndices()[0],
      &K(0,0),ADD_VALUES
    ); CHKERRQ(ierr);
    if(row_side != col_side || row_type != col_type) {
      transK.resize(nb_col,nb_row);
      noalias(transK) = trans( K );
      ierr = MatSetValues(
        (getFEMethod()->ts_B),
        nb_col,&col_data.getIndices()[0],
        nb_row,&row_data.getIndices()[0],
        &transK(0,0),ADD_VALUES
      ); CHKERRQ(ierr);
    }


  } catch (const std::exception& ex) {
    std::ostringstream ss;
    ss << "throw in method: " << ex.what() << std::endl;
    SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::UpdateAndControl::preProcess() {
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = mField.set_other_local_ghost_vector(
    problemPtr,tempName,rateName,ROW,ts_u_t,INSERT_VALUES,SCATTER_REVERSE
  ); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::UpdateAndControl::postProcess() {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::TimeSeriesMonitor::postProcess() {
  PetscFunctionBegin;
  PetscErrorCode ierr;

  ierr = mField.set_global_ghost_vector(
    problemPtr,ROW,ts_u,INSERT_VALUES,SCATTER_REVERSE
  ); CHKERRQ(ierr);

  BitRefLevel proble_bit_level = problemPtr->getBitRefLevel();

  SeriesRecorder *recorder_ptr = NULL;
  ierr = mField.query_interface(recorder_ptr); CHKERRQ(ierr);
  ierr = recorder_ptr->record_begin(seriesName); CHKERRQ(ierr);
  ierr = recorder_ptr->record_field(seriesName,tempName,proble_bit_level,mask); CHKERRQ(ierr);
  ierr = recorder_ptr->record_end(seriesName,ts_t); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::addThermalElements(const std::string field_name,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  ErrorCode rval;

  ierr = mField.add_finite_element("THERMAL_FE",MF_ZERO); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_row("THERMAL_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_col("THERMAL_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_data("THERMAL_FE",field_name); CHKERRQ(ierr);
  if(mField.check_field(mesh_nodals_positions)) {
    ierr = mField.modify_finite_element_add_field_data("THERMAL_FE",mesh_nodals_positions); CHKERRQ(ierr);
  }

  //takes skin of block of entities
  //Skinner skin(&mField.get_moab());
  // loop over all blocksets and get data which name is FluidPressure
  for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(mField,BLOCKSET|MAT_THERMALSET,it)) {

    Mat_Thermal temp_data;
    ierr = it->get_attribute_data_structure(temp_data); CHKERRQ(ierr);

    setOfBlocks[it->get_msId()].cOnductivity_mat.resize(3,3); //(3X3) conductivity matrix
    setOfBlocks[it->get_msId()].cOnductivity_mat.clear();
    setOfBlocks[it->get_msId()].cOnductivity_mat(0,0)=temp_data.data.Conductivity;
    setOfBlocks[it->get_msId()].cOnductivity_mat(1,1)=temp_data.data.Conductivity;
    setOfBlocks[it->get_msId()].cOnductivity_mat(2,2)=temp_data.data.Conductivity;
    //setOfBlocks[it->get_msId()].cOnductivity = temp_data.data.Conductivity;

    setOfBlocks[it->get_msId()].cApacity = temp_data.data.HeatCapacity;
    rval = mField.get_moab().get_entities_by_type(it->meshset,MBTET,setOfBlocks[it->get_msId()].tEts,true); CHKERRQ_MOAB(rval);
    ierr = mField.add_ents_to_finite_element_by_TETs(setOfBlocks[it->get_msId()].tEts,"THERMAL_FE"); CHKERRQ(ierr);

  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::addThermalFluxElement(const std::string field_name,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  ErrorCode rval;

  ierr = mField.add_finite_element("THERMAL_FLUX_FE",MF_ZERO); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_row("THERMAL_FLUX_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_col("THERMAL_FLUX_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_data("THERMAL_FLUX_FE",field_name); CHKERRQ(ierr);
  if(mField.check_field(mesh_nodals_positions)) {
    ierr = mField.modify_finite_element_add_field_data("THERMAL_FLUX_FE",mesh_nodals_positions); CHKERRQ(ierr);
  }

  for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(mField,SIDESET|HEATFLUXSET,it)) {
    ierr = it->get_bc_data_structure(setOfFluxes[it->get_msId()].dAta); CHKERRQ(ierr);
    rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfFluxes[it->get_msId()].tRis,true); CHKERRQ_MOAB(rval);
    ierr = mField.add_ents_to_finite_element_by_TRIs(setOfFluxes[it->get_msId()].tRis,"THERMAL_FLUX_FE"); CHKERRQ(ierr);
  }

  //this is alternative method for setting boundary conditions, to bypass bu in cubit file reader.
  //not elegant, but good enough
  for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
    if(it->getName().compare(0,9,"HEAT_FLUX") == 0) {
      std::vector<double> data;
      ierr = it->get_attributes(data); CHKERRQ(ierr);
      if(data.size()!=1) {
        SETERRQ(PETSC_COMM_SELF,1,"Data inconsistency");
      }
      strcpy(setOfFluxes[it->get_msId()].dAta.data.name,"HeatFlu");
      setOfFluxes[it->get_msId()].dAta.data.flag1 = 1;
      setOfFluxes[it->get_msId()].dAta.data.value1 = data[0];
      //std::cerr << setOfFluxes[it->get_msId()].dAta << std::endl;
      rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfFluxes[it->get_msId()].tRis,true); CHKERRQ_MOAB(rval);
      ierr = mField.add_ents_to_finite_element_by_TRIs(setOfFluxes[it->get_msId()].tRis,"THERMAL_FLUX_FE"); CHKERRQ(ierr);
    }
  }


  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::addThermalConvectionElement(const std::string field_name,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  ErrorCode rval;

  ierr = mField.add_finite_element("THERMAL_CONVECTION_FE",MF_ZERO); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_row("THERMAL_CONVECTION_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_col("THERMAL_CONVECTION_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_data("THERMAL_CONVECTION_FE",field_name); CHKERRQ(ierr);
  if(mField.check_field(mesh_nodals_positions)) {
    ierr = mField.modify_finite_element_add_field_data("THERMAL_CONVECTION_FE",mesh_nodals_positions); CHKERRQ(ierr);
  }

  //this is alternative method for setting boundary conditions, to bypass bu in cubit file reader.
  //not elegant, but good enough
  for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
    if(it->getName().compare(0,10,"CONVECTION") == 0) {

      std::vector<double> data;
      ierr = it->get_attributes(data); CHKERRQ(ierr);
      if(data.size()!=2) {
        SETERRQ(PETSC_COMM_SELF,1,"Data inconsistency");
      }
      setOfConvection[it->get_msId()].cOnvection = data[0];
      setOfConvection[it->get_msId()].tEmperature = data[1];
      //std::cerr << setOfFluxes[it->get_msId()].dAta << std::endl;
      rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfConvection[it->get_msId()].tRis,true); CHKERRQ_MOAB(rval);
      ierr = mField.add_ents_to_finite_element_by_TRIs(setOfConvection[it->get_msId()].tRis,"THERMAL_CONVECTION_FE"); CHKERRQ(ierr);

    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::addThermalRadiationElement(const std::string field_name,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  ErrorCode rval;

  ierr = mField.add_finite_element("THERMAL_RADIATION_FE",MF_ZERO); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_row("THERMAL_RADIATION_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_col("THERMAL_RADIATION_FE",field_name); CHKERRQ(ierr);
  ierr = mField.modify_finite_element_add_field_data("THERMAL_RADIATION_FE",field_name); CHKERRQ(ierr);
  if(mField.check_field(mesh_nodals_positions)) {
    ierr = mField.modify_finite_element_add_field_data("THERMAL_RADIATION_FE",mesh_nodals_positions); CHKERRQ(ierr);
  }

  //this is alternative method for setting boundary conditions, to bypass bu in cubit file reader.
  //not elegant, but good enough
  for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
    if(it->getName().compare(0,9,"RADIATION") == 0) {
      std::vector<double> data;
      ierr = it->get_attributes(data); CHKERRQ(ierr);
      if(data.size()!=3) {
        SETERRQ(PETSC_COMM_SELF,1,"Data inconsistency");
      }
      setOfRadiation[it->get_msId()].sIgma = data[0];
      setOfRadiation[it->get_msId()].eMissivity = data[1];
      //setOfRadiation[it->get_msId()].aBsorption = data[2];
      setOfRadiation[it->get_msId()].aMbienttEmp = data[2];
      //std::cerr << setOfFluxes[it->get_msId()].dAta << std::endl;
      rval = mField.get_moab().get_entities_by_type(it->meshset,MBTRI,setOfRadiation[it->get_msId()].tRis,true); CHKERRQ_MOAB(rval);
      ierr = mField.add_ents_to_finite_element_by_TRIs(setOfRadiation[it->get_msId()].tRis,"THERMAL_RADIATION_FE"); CHKERRQ(ierr);

    }
  }

  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::setThermalFiniteElementRhsOperators(string field_name,Vec &F) {
  PetscFunctionBegin;
  std::map<int,BlockData>::iterator sit = setOfBlocks.begin();
  feRhs.getOpPtrVector().push_back(new OpGetGradAtGaussPts(field_name,commonData));
  for(;sit!=setOfBlocks.end();sit++) {
    //add finite element
    feRhs.getOpPtrVector().push_back(new OpThermalRhs(field_name,F,sit->second,commonData));
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::setThermalFiniteElementLhsOperators(string field_name,Mat A) {
  PetscFunctionBegin;
  std::map<int,BlockData>::iterator sit = setOfBlocks.begin();
  for(;sit!=setOfBlocks.end();sit++) {
    //add finite elemen
    feLhs.getOpPtrVector().push_back(new OpThermalLhs(field_name,A,sit->second,commonData));
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::setThermalFluxFiniteElementRhsOperators(string field_name,Vec &F,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;
  bool hoGeometry = false;
  if(mField.check_field(mesh_nodals_positions)) {
    hoGeometry = true;
  }
  std::map<int,FluxData>::iterator sit = setOfFluxes.begin();
  for(;sit!=setOfFluxes.end();sit++) {
    //add finite element
    feFlux.getOpPtrVector().push_back(new OpHeatFlux(field_name,F,sit->second,hoGeometry));
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::setThermalConvectionFiniteElementRhsOperators(string field_name,Vec &F,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;
  bool hoGeometry = false;
  if(mField.check_field(mesh_nodals_positions)) {
    hoGeometry = true;
  }
  std::map<int,ConvectionData>::iterator sit = setOfConvection.begin();
  for(;sit!=setOfConvection.end();sit++) {
    //add finite element
    feConvectionRhs.getOpPtrVector().push_back(new OpGetTriTemperatureAtGaussPts(field_name,commonData));
    feConvectionRhs.getOpPtrVector().push_back(new OpConvectionRhs(field_name,F,sit->second,commonData,hoGeometry));
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::setThermalConvectionFiniteElementLhsOperators(string field_name,Mat A,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;
  bool hoGeometry = false;
  if(mField.check_field(mesh_nodals_positions)) {
    hoGeometry = true;
  }
  std::map<int,ConvectionData>::iterator sit = setOfConvection.begin();
  for(;sit!=setOfConvection.end();sit++) {
    //add finite element
    feConvectionLhs.getOpPtrVector().push_back(new OpConvectionLhs(field_name,A,sit->second,hoGeometry));
  }
  PetscFunctionReturn(0);
}

PetscErrorCode ThermalElement::setTimeSteppingProblem(string field_name,string rate_name,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;

  bool hoGeometry = false;
  if(mField.check_field(mesh_nodals_positions)) {
    hoGeometry = true;
  }

  {
    std::map<int,BlockData>::iterator sit = setOfBlocks.begin();
    for(;sit!=setOfBlocks.end();sit++) {
      //add finite element
      //those methods are to calculate matrices on Lhs
      //  feLhs.getOpPtrVector().push_back(new OpGetTetTemperatureAtGaussPts(field_name,commonData));
      feLhs.getOpPtrVector().push_back(new OpThermalLhs(field_name,sit->second,commonData));
      feLhs.getOpPtrVector().push_back(new OpHeatCapacityLhs(field_name,sit->second,commonData));
      //those methods are to calculate vectors on Rhs
      feRhs.getOpPtrVector().push_back(new OpGetTetTemperatureAtGaussPts(field_name,commonData));
      feRhs.getOpPtrVector().push_back(new OpGetTetRateAtGaussPts(rate_name,commonData));
      feRhs.getOpPtrVector().push_back(new OpGetGradAtGaussPts(field_name,commonData));
      feRhs.getOpPtrVector().push_back(new OpThermalRhs(field_name,sit->second,commonData));
      feRhs.getOpPtrVector().push_back(new OpHeatCapacityRhs(field_name,sit->second,commonData));
    }
  }

  //Flux
  {
    std::map<int,FluxData>::iterator sit = setOfFluxes.begin();
    for(;sit!=setOfFluxes.end();sit++) {
      feFlux.getOpPtrVector().push_back(new OpHeatFlux(field_name,sit->second,hoGeometry));
    }
  }

  // Convection
  {
    std::map<int,ConvectionData>::iterator sit = setOfConvection.begin();
    for(;sit!=setOfConvection.end();sit++) {
      feConvectionRhs.getOpPtrVector().push_back(new OpGetTriTemperatureAtGaussPts(field_name,commonData));
      feConvectionRhs.getOpPtrVector().push_back(new OpConvectionRhs(field_name,sit->second,commonData,hoGeometry));
    }
  }
  {
    std::map<int,ConvectionData>::iterator sit = setOfConvection.begin();
    for(;sit!=setOfConvection.end();sit++) {
      feConvectionLhs.getOpPtrVector().push_back(new OpConvectionLhs(field_name,sit->second,hoGeometry));
    }
  }

  //Radiation
  {
    std::map<int,RadiationData>::iterator sit = setOfRadiation.begin();
    for(;sit!=setOfRadiation.end();sit++) {
      feRadiationRhs.getOpPtrVector().push_back(new OpGetTriTemperatureAtGaussPts(field_name,commonData));
      feRadiationRhs.getOpPtrVector().push_back(new OpRadiationRhs(field_name,sit->second,commonData,hoGeometry));
    }
  }
  {
    std::map<int,RadiationData>::iterator sit = setOfRadiation.begin();
    for(;sit!=setOfRadiation.end();sit++) {
      feRadiationLhs.getOpPtrVector().push_back(new OpGetTriTemperatureAtGaussPts(field_name,commonData));
      feRadiationLhs.getOpPtrVector().push_back(new OpRadiationLhs(field_name,sit->second,commonData,hoGeometry));
    }
  }


  PetscFunctionReturn(0);
}


PetscErrorCode ThermalElement::setTimeSteppingProblem(TsCtx &ts_ctx,string field_name,string rate_name,const std::string mesh_nodals_positions) {
  PetscFunctionBegin;

  PetscErrorCode ierr;
  ierr = setTimeSteppingProblem(field_name,rate_name,mesh_nodals_positions); CHKERRQ(ierr);

  //rhs
  TsCtx::loops_to_do_type& loops_to_do_Rhs = ts_ctx.get_loops_to_do_IFunction();
  loops_to_do_Rhs.push_back(TsCtx::loop_pair_type("THERMAL_FE",&feRhs));
  loops_to_do_Rhs.push_back(TsCtx::loop_pair_type("THERMAL_FLUX_FE",&feFlux));
  loops_to_do_Rhs.push_back(TsCtx::loop_pair_type("THERMAL_CONVECTION_FE",&feConvectionRhs));
  loops_to_do_Rhs.push_back(TsCtx::loop_pair_type("THERMAL_RADIATION_FE",&feRadiationRhs));

  //lhs
  TsCtx::loops_to_do_type& loops_to_do_Mat = ts_ctx.get_loops_to_do_IJacobian();
  loops_to_do_Mat.push_back(TsCtx::loop_pair_type("THERMAL_FE",&feLhs));
  loops_to_do_Mat.push_back(TsCtx::loop_pair_type("THERMAL_CONVECTION_FE",&feConvectionLhs));
  loops_to_do_Mat.push_back(TsCtx::loop_pair_type("THERMAL_RADIATION_FE",&feRadiationLhs));
  //monitor
  //TsCtx::loops_to_do_type& loops_to_do_Monitor = ts_ctx.get_loops_to_do_Monitor();

  PetscFunctionReturn(0);
}
