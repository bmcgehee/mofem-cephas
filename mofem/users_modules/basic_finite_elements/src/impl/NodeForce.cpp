/** \file NodeForce.cpp
  \ingroup mofem_static_boundary_conditions
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
using namespace MoFEM;
#include <MethodForForceScaling.hpp>
#include <SurfacePressure.hpp>
#include <NodalForce.hpp>

using namespace boost::numeric;

NodalForce::MyFE::MyFE(MoFEM::Interface &m_field): VertexElementForcesAndSourcesCore(m_field) {

}

NodalForce::OpNodalForce::OpNodalForce(const std::string field_name,Vec _F,bCForce &data,
  boost::ptr_vector<MethodForForceScaling> &methods_op,bool use_snes_f):
  VertexElementForcesAndSourcesCore::UserDataOperator(field_name,ForcesAndSurcesCore::UserDataOperator::OPROW),
  F(_F),
  useSnesF(use_snes_f),
  dAta(data),
  methodsOp(methods_op) {}

  PetscErrorCode NodalForce::OpNodalForce::doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
    PetscFunctionBegin;

    if(data.getIndices().size()==0) PetscFunctionReturn(0);
    EntityHandle ent = getNumeredEntFiniteElementPtr()->getEnt();
    if(dAta.nOdes.find(ent)==dAta.nOdes.end()) PetscFunctionReturn(0);

    PetscErrorCode ierr;

    const FENumeredDofEntity *dof_ptr;
    ierr = getNumeredEntFiniteElementPtr()->getRowDofsByPetscGlobalDofIdx(data.getIndices()[0],&dof_ptr); CHKERRQ(ierr);
    int rank = dof_ptr->getNbOfCoeffs();

    if(data.getIndices().size()!=(unsigned int)rank) {
      SETERRQ(PETSC_COMM_SELF,1,"data inconsistency");
    }

    Nf.resize(3);
    for(int rr = 0;rr<rank;rr++) {
      if(rr == 0) {
        Nf[0] = dAta.data.data.value3*dAta.data.data.value1;
      } else if(rr == 1) {
        Nf[1] = dAta.data.data.value4*dAta.data.data.value1;
      } else if(rr == 2) {
        Nf[2] = dAta.data.data.value5*dAta.data.data.value1;
      } else {
        SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconsistency");
      }
    }

    ierr = MethodForForceScaling::applyScale(getFEMethod(),methodsOp,Nf); CHKERRQ(ierr);
    Vec myF = F;
    if(useSnesF || F == PETSC_NULL) {
      switch (getFEMethod()->ts_ctx) {
        case FEMethod::CTX_TSSETIFUNCTION: {
          const_cast<FEMethod*>(getFEMethod())->snes_ctx = FEMethod::CTX_SNESSETFUNCTION;
          const_cast<FEMethod*>(getFEMethod())->snes_x = getFEMethod()->ts_u;
          const_cast<FEMethod*>(getFEMethod())->snes_f = getFEMethod()->ts_F;
          break;
        }
        default:
        break;
      }
      myF = getFEMethod()->snes_f;
    }
    ierr = VecSetValues(
      myF,data.getIndices().size(),
      &data.getIndices()[0],&Nf[0],ADD_VALUES
    ); CHKERRQ(ierr);

    PetscFunctionReturn(0);
  }

  PetscErrorCode NodalForce::addForce(const std::string field_name,Vec F,int ms_id,bool use_snes_f) {
    PetscFunctionBegin;
    PetscErrorCode ierr;
    ErrorCode rval;
    const CubitMeshSets *cubit_meshset_ptr;
    ierr = mField.get_cubit_msId(ms_id,NODESET,&cubit_meshset_ptr); CHKERRQ(ierr);
    ierr = cubit_meshset_ptr->get_bc_data_structure(mapForce[ms_id].data); CHKERRQ(ierr);
    rval = mField.get_moab().get_entities_by_type(cubit_meshset_ptr->meshset,MBVERTEX,mapForce[ms_id].nOdes,true); CHKERRQ_MOAB(rval);
    fe.getOpPtrVector().push_back(new OpNodalForce(field_name,F,mapForce[ms_id],methodsOp,use_snes_f));
    PetscFunctionReturn(0);
  }

  MetaNodalForces::TagForceScale::TagForceScale(MoFEM::Interface &m_field): mField(m_field) {
    ErrorCode rval;
    double def_scale = 1.;
    const EntityHandle root_meshset = mField.get_moab().get_root_set();
    rval = mField.get_moab().tag_get_handle("_LoadFactor_Scale_",1,MB_TYPE_DOUBLE,thScale,MB_TAG_CREAT|MB_TAG_EXCL|MB_TAG_MESH,&def_scale);
    if(rval == MB_ALREADY_ALLOCATED) {
      rval = mField.get_moab().tag_get_by_ptr(thScale,&root_meshset,1,(const void**)&sCale); MOAB_THROW(rval);
    } else {
      MOAB_THROW(rval);
      rval = mField.get_moab().tag_set_data(thScale,&root_meshset,1,&def_scale); MOAB_THROW(rval);
      rval = mField.get_moab().tag_get_by_ptr(thScale,&root_meshset,1,(const void**)&sCale); MOAB_THROW(rval);
    }
  }

  PetscErrorCode MetaNodalForces::TagForceScale::scaleNf(const FEMethod *fe,ublas::vector<FieldData> &Nf) {
    PetscFunctionBegin;
    Nf *= *sCale;
    PetscFunctionReturn(0);
  }
