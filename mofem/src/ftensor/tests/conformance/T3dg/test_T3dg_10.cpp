#include <iostream>
#include "../../../src/FTensor.hpp"
#include "../test_for_zero.hpp"
using namespace FTensor;
using namespace std;

void test_T3dg_10(Tensor3_dg<double,3,3> &t3dg_1)
{
  Index<'i',3> i;
  Index<'j',3> j;
  Index<'k',3> k;
  Index<'l',3> l;
  Index<'m',3> m;
  Index<'n',3> n;

  Number<0> N0;
  Number<1> N1;
  Number<2> N2;

  /* Tensor3_dg tests */

  t3dg_1(i,j,k)=10;
  test_for_zero(t3dg_1(0,0,0) - 10
		,"T3dg=T(0,0,0)");
  test_for_zero(t3dg_1(0,0,1) - 10
		,"T3dg=T(0,0,1)");
  test_for_zero(t3dg_1(0,0,2) - 10
		,"T3dg=T(0,0,2)");
  test_for_zero(t3dg_1(0,1,0) - 10
		,"T3dg=T(0,1,0)");
  test_for_zero(t3dg_1(0,1,1) - 10
		,"T3dg=T(0,1,1)");
  test_for_zero(t3dg_1(0,1,2) - 10
		,"T3dg=T(0,1,2)");
  test_for_zero(t3dg_1(0,2,0) - 10
		,"T3dg=T(0,2,0)");
  test_for_zero(t3dg_1(0,2,1) - 10
		,"T3dg=T(0,2,1)");
  test_for_zero(t3dg_1(0,2,2) - 10
		,"T3dg=T(0,2,2)");
  test_for_zero(t3dg_1(1,0,0) - 10
		,"T3dg=T(1,0,0)");
  test_for_zero(t3dg_1(1,0,1) - 10
		,"T3dg=T(1,0,1)");
  test_for_zero(t3dg_1(1,0,2) - 10
		,"T3dg=T(1,0,2)");
  test_for_zero(t3dg_1(1,1,0) - 10
		,"T3dg=T(1,1,0)");
  test_for_zero(t3dg_1(1,1,1) - 10
		,"T3dg=T(1,1,1)");
  test_for_zero(t3dg_1(1,1,2) - 10
		,"T3dg=T(1,1,2)");
  test_for_zero(t3dg_1(1,2,0) - 10
		,"T3dg=T(1,2,0)");
  test_for_zero(t3dg_1(1,2,1) - 10
		,"T3dg=T(1,2,1)");
  test_for_zero(t3dg_1(1,2,2) - 10
		,"T3dg=T(1,2,2)");
  test_for_zero(t3dg_1(2,0,0) - 10
		,"T3dg=T(2,0,0)");
  test_for_zero(t3dg_1(2,0,1) - 10
		,"T3dg=T(2,0,1)");
  test_for_zero(t3dg_1(2,0,2) - 10
		,"T3dg=T(2,0,2)");
  test_for_zero(t3dg_1(2,1,0) - 10
		,"T3dg=T(2,1,0)");
  test_for_zero(t3dg_1(2,1,1) - 10
		,"T3dg=T(2,1,1)");
  test_for_zero(t3dg_1(2,1,2) - 10
		,"T3dg=T(2,1,2)");
  test_for_zero(t3dg_1(2,2,0) - 10
		,"T3dg=T(2,2,0)");
  test_for_zero(t3dg_1(2,2,1) - 10
		,"T3dg=T(2,2,1)");
  test_for_zero(t3dg_1(2,2,2) - 10
		,"T3dg=T(2,2,2)");
}

