#include <iostream>
#include "../../src/FTensor.hpp"
using namespace FTensor;
using namespace std;

#define FTENOR_MOFEMTESTING 1

#include "test_T0.hpp"
#include "test_T1.hpp"
#include "test_T2.hpp"
#include "test_T2s.hpp"
#include "test_T3.hpp"
#include "test_T3dg.hpp"
#include "test_T3ch.hpp"
#include "test_T3as.hpp"
#include "test_T4.hpp"
#include "test_T4ddg.hpp"
#include "test_T4R.hpp"
#include "test_T4.hpp"

int main()
{
  double t01(12), t02(14);

  Tensor0<double *> t0_1(&t01), t0_2(&t02);
  Tensor1<double,3> t1_1(1,2,3), t1_2(4,5,6), t1_3(7,8,9);
  Tensor2<double,3,3> t2_1(10,20,30,40,50,60,70,80,90),
    t2_2(11,21,31,41,51,61,71,81,91),
    t2_3(12,22,32,42,52,62,72,82,92);
  Tensor2_symmetric<double,3> t2s_1(13,23,33,43,53,63),
    t2s_2(14,24,34,44,54,64), t2s_3(15,25,35,45,55,65);
  Tensor3<double,3,3,3> t3_1(333,334,335,336,337,338,339,340,341,
                             433,434,435,436,437,438,439,440,441,
                             533,534,535,536,537,538,539,540,541);
  Tensor3_dg<double,3,3> t3dg_1(101,102,103,104,105,106,107,108,109,110,111,
				112,113,114,115,116,117,118),
    t3dg_2(201,202,203,204,205,206,207,208,209,210,211,
	   212,213,214,215,216,217,218),
    t3dg_3(301,302,303,304,305,306,307,308,309,310,311,
	   312,313,314,315,316,317,318);
  Tensor3_christof<double,3,3>
    t3ch_1(401,402,403,404,405,406,407,408,409,410,411,
	   412,413,414,415,416,417,418),
    t3ch_2(501,502,503,504,505,506,507,508,509,510,511,
	   512,513,514,515,516,517,518),
    t3ch_3(601,602,603,604,605,606,607,608,609,610,611,
	   612,613,614,615,616,617,618);
  Tensor3_antisymmetric<double,3,3>
    t3as_1(701,702,703,704,705,706,707,708,709),
    t3as_2(801,802,803,804,805,806,807,808,809),
    t3as_3(901,902,903,904,905,906,907,908,909);
  Tensor4<double,3,3,3,3> t4_1(
    1111,1112,1113,1121,1122,1123,1131,1132,1133,
    1211,1212,1213,1221,1222,1223,1231,1232,1233,
    1311,1312,1313,1321,1322,1323,1331,1332,1333,
    2111,2112,2113,2121,2122,2123,2131,2132,2133,
    2211,2212,2213,2221,2222,2223,2231,2232,2233,
    2311,2312,2313,2321,2322,2323,2331,2332,2333,
    3111,3112,3113,3121,3122,3123,3131,3132,3133,
    3211,3212,3213,3221,3222,3223,3231,3232,3233,
    3311,3312,3313,3321,3322,3323,3331,3332,3333
  );


  test_T0(13,t0_1,t0_2);
  test_T1(t1_1,t1_2);
  test_T2(t1_1,t1_2,t2_1,t2_2,t2_3);
  test_T2s(t1_1,t1_2,t2_1,t2_2,t2s_1,t2s_2,t2s_3);
  test_T3(t1_1,t1_2,t2_2,t2_3,t2s_2,t2s_3,t3_1,t3dg_2);
  test_T3dg(t1_1,t1_2,t2_1,t2_2,t2s_1,t2s_2,t2s_3,t3dg_1,t3dg_2,t3dg_3);
  test_T3ch(13,t0_1,t0_2,t1_1,t1_2,t2_1,t2_2,t2_3,t2s_1,t2s_2,t2s_3,
	    t3dg_1,t3dg_2,t3dg_3,t3ch_1,t3ch_2,t3ch_3);
  test_T3as(t1_2,t2_2,t3dg_1,t3dg_2,t3as_1,t3as_2,t3as_3);
  test_T4ddg(t1_1,t1_2,t2_1,t2_2,t2_3,t2s_1,t2s_2,t2s_3,t3dg_1,t3dg_2,t3dg_3);
  test_T4R();
  test_T4(t1_1,t2_1,t2s_1,t4_1);
}
