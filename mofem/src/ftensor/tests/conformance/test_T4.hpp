void test_T4(
  const Tensor1<double,3> &t1_1,
  const Tensor2<double,3,3> &t2_1,
  const Tensor2_symmetric<double,3>  &t2s_1,
  const Tensor4<double,3,3,3,3> &t4_1
);
